import React, { Component } from 'react';
import { Button, message } from "antd"
import ListCart from '../Components/cart/list';
import { connect } from 'react-redux';

const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog
  };
};

class CartPage extends Component {

  state = {
    items: [],
    email: '',
    isDisable: true
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem('user-data');
    const email = jsonStr && JSON.parse(jsonStr).email;
    this.setState({ email });
    const jsonCartStr = localStorage.getItem(`beer-ja-list-cart-${email}`);
    if (jsonCartStr) {
      const items = jsonCartStr && JSON.parse(jsonCartStr);
      if (items.length > 0) {
        this.setState({ items: items, isDisable: false });
      }
    }
  }

  getItems = () => {
    const jsonStr = localStorage.getItem('user-data');
    const email = jsonStr && JSON.parse(jsonStr).email;
    const jsonFavStr = localStorage.getItem(`beer-ja-list-cart-${email}`);
    if (jsonFavStr) {
      const items = jsonFavStr && JSON.parse(jsonFavStr);
      return items;
    }
  };

  onClickCheckout = () => {
    localStorage.setItem(
      `beer-ja-list-cart-${this.state.email}`,
      JSON.stringify([])
    );
    this.setState({ items: [], isDisable: true });
    message.success('You checkout successfully', 1, () => {
      this.props.history.replace('/');
    });
  };

  render() {
    console.log(this.props.isShowDialog);
    return (
      <div
        style={{
          padding: '16px',
          marginTop: 64,
          minHeight: '600px'
        }} 
      >
        <ListCart items={this.getItems()} />
        <Button
            disabled={this.state.isDisable}
            type="primary"
            style={{
              position: 'absolute',
              top: '16%',
              right: '40px',
              transform: 'translateY(-50%)'
            }}
            onClick={this.onClickCheckout}
          >
            Checkout
          </Button>
      </div>
    );
  }
}

export default connect(mapStateToProps)(CartPage);
