import React, { Component } from 'react';
import { Button, Avatar, Icon } from 'antd';

class ProfilePage extends Component {
    state = {
        email: '',
        isLoading: false,
        imageUrl: ''
    };

    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data');
        const email = jsonStr && JSON.parse(jsonStr).email;
        var imageUrl = jsonStr && JSON.parse(jsonStr).imageUrl;
        if (!imageUrl) {
            imageUrl = 'https://www.freeiconspng.com/uploads/person-icon-8.png';
        }
        this.setState({ email, imageUrl });
    }

    onClickLogout = () => {
        this.setState({ isLoading: true });
        localStorage.setItem(
            'user-data',
            JSON.stringify({
                isLoggedIn: false
            })
        );
        setTimeout(() => {
            this.setState({ isLoading: false });
            this.props.history.push('/');
        }, 1000);
    };

    render() {
        return (
            <div
                style={{
                    padding: '16px',
                    marginTop: 64,
                    minHeight: '600px',
                    justifyContent: 'center',
                    alignItems: 'center',
                    display: 'flex'
                }}
            >
                <div>
                    <Avatar size={100} src={this.state.imageUrl} />
                    <h3>Email: {this.state.email}</h3>
                    <br />
                    <Button
                        style={{ width: '100%' }}
                        type="danger"
                        loading={this.state.isLoading}
                        onClick={this.onClickLogout}
                    >

                        LOGOUT
                    </Button>
                </div>
            </div>
        );
    }
}

export default ProfilePage;
