import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Login from './Login'
import Main from './Main'
import Register from './Register'

function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route component={Main} />
            </Switch>
        </div>
    )
}

export default Routes